# mezame

「めざめ」awaken

# Inspiration

A customizable terminal timer

# Requirements (9)

0. bash
1. echo (write arguments to output, with endline)
2. fortune (optional); motivate (optional)
3. osascript (volume regulator)
4. posix (terminal standards)
5. printf (write arguments to output, with %)
6. read (write arguments to output, without endline)
7. sleep (suspend execution for an interval of time)
8. sox (sound exchange); mpv (media player); aplay (apple audio file play)
9. tput (initialize terminal)

# Method

nested loop

while loop

prompt loop

duration loop

timer loop

# Screenshot

![mezame](mezame.png?raw=true "mezame")