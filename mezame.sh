#!/bin/bash

# v2020-05-28-th

# Inspiration
## A customizable terminal timer (macos)

# Requirements (9)
## 0. bash
## 1. echo (write arguments to output, with endline)
## 2. fortune (optional); motivate (optional)
## 3. osascript (volume regulator, macos)
## 4. posix (terminal standards)
## 5. printf (write arguments to output, with %)
## 6. read (write arguments to output, without endline)
## 7. sleep (suspend execution for an interval of time)
## 8. sox (sound exchange); mpv (media player); aplay (apple audio file play)
## 9. tput (initialize terminal)

# Method: comments (0) - (9)
## nested loop
## while loop
## prompt loop
## duration loop
## timer loop

# Todo-A
## <> Bookmark; ' Quickmark
## - [ ] osascript (macos) => amixer; pulseaudio

# Todo-B
## - [ ] audio player: make command operating system agnostic?

# Todo-C
## - [ ] Language: make ./mezame.sh script language agnostic?

# Colors: red, green, yellow
## syntax: echo '"${COLOR}"' or read -p '$'\e[0m (white)
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
RESET='\033[0m'

## (0) tput: Initialize, hide and clear cursor
tput init
tput civis
tput clear

## (0) date: 00:00 0000-00-00-abc
date +"%H:%m %Y-%m-%d-%a"

## (0) initialize: set variables to zero.

## (0) initialize: iteration i
i=0

## (0) initialize: hour; min; sec
hour=0
min=0
sec=0

## (0) initialize: duration dur; session; total
dur=0
session=0
total=0

## (1) Welcome message
echo -ne '\n「'"${RED}"'め'"${GREEN}"'ざ'"${YELLOW}"め"${RESET}"'」''\n'

## (2) Continue studying? loop
while true; do

	## (0) initialize echo -ne: do not print trailing newline character; enable interpretation of backslash-escaped characters in each String
	echo -ne "${RESET}"

	## Continue studying? Yes or No:
	## read -p does not process special escapes, so specify the colors literally, e.g.'$'\e[0m (white)
	read -p $'\e[33m\nContinue studying? \e[32mYes\e[0m or '$'\e[31mNo\e[0m: ' input

	## (2) Continue studying? loop: input choices: Yes; No; Else
	case $input in

		## (2) Continue studying? No
		[nN]*)

			echo -ne "${YELLOW}" '\nOk, exiting.\n'"${RESET}"

			## (7) Iteration: Calculate & add $total of all study $session $dur (durations).
			## Final total: Studied for $total mintue(s), consisting of $i session(s).
			echo -ne "${YELLOW}"'\nFinal total: '"${GREEN}"'Studied for '"${RED}"$total 'minute'\('s'\)'s, '"${RED}""${GREEN}"'consisting of '"${RED}"$i' session'\('s'\)'.'"${GREEN}"'\n\n'"${RESET}"

			## (0) tput: Set the cursor to its normal state
			tput cnorm

			exit 1
			;;

		## (2) Continue studying? Yes
		*)

			echo -ne "${GREEN}" '\nContinuing.\n' "${RESET}"

			## (3) Duration? (minutes): hour=0; min=read; second=0

			hour=0

			read -p $'\e[33m\nDuration? (minutes)\e[0m: ' min

			## (3) Duration (minutes): Only integers allowed.
			if ! [[ "$min" =~ ^[0-9]+$ ]];
			then
				echo -ne "${RED}" '\nInvalid input: '"${GREEN}"Integer 0-9.'\n'"${RESET}"
			else

			## (3) Duration (minutes): Save input $min as $dur (duration of this study session).
			dur=$min

			## (3) Duration (minutes): Seconds are less relevant than minutes (too precise).
			sec=0

			## (3) Duration (minutes): Save iteration $i as $session.
			session=0

			## (4) Change song? Yes or No.
			read -p $'\e[33m\nChange song? \e[32mYes\e[0m or '$'\e[31mNo\e[0m: ' song

			case $song in

				## (4) Yes. Song of choice? (absolute or relative path):
				[yY]*)
					read -p $'\e[33m\nSong of choice? \e[0m(absolute or relative path): ' song
					;;

				## (4) No. Setting default song.
				*)
					song="/Users/mcb/.config/mezame/01 Euterpe ~Silence~.flac"
					echo -ne "${GREEN}"'\n'Setting default song"${RESET}": $song
					echo -ne '\n'
			esac

			## (5) Change volume? Volume? 0-7 (1.1 = 15%, level 3)
			read -p $'\e[33m\nVolume? (0 - 15)\e[0m: ' volume
			echo -ne '\n'

			## (5) Volume (minutes): Only integers allowed.
			if ! [[ "$volume" =~ ^[0-9]+$ ]];
			then
				volume=1.2
				osascript -e "set Volume "$volume
				echo -ne "${GREEN}"'Setting default volume'"${RESET}"': 1.2 (5%)\n'
			else
				osascript -e "set Volume "$volume
				echo -ne "${YELLOW}"'Setting volume of choice'"${RESET}"': '$volume
				echo -ne '\n'
			fi

			## (6) Quote
			read -p $'\e[33m\nChange message? \e[31mCustom\e[0m, '$'\e[32mFortune\e[0m or '$'\e[33mMotivate\e[0m: ' quote

			case $quote in

				## (6) Yes. Custom quote.
				[cC]*)
					read -p $'\e[31m\nQuote of choice\e[0m: ' quote
					echo -ne "\n${RED}"$quote"${RESET}"
					echo -ne "${RESET}"'\n'
					;;

				## (6) Yes. Fortune quote.
				[fF]*)
					echo -ne '\n'"${GREEN}"'Quote of fortune'"${RESET}"':'
					echo -ne '\n\n'
					fortune
					;;

				## (6) Yes. Motivate quote.
				[mM]*)
					echo -ne '\n'"${YELLOW}"'Quote of motivation'"${RESET}"':'
					echo -ne '\n\n'
					motivate
					;;

				## (6) No. Default quote.
				*)
					echo -ne '\n'"${GREEN}"'Setting default quote.'"${RESET}"'\n'
			esac

				## (7) Timer (HH:mm:ss): Count down from $input in $min (minutes).
				echo -ne '\n'
				while [ $hour -ge 0 ]; do
					while [ $min -ge 0 ]; do
						while [ $sec -ge 0 ]; do
						echo -ne "${GREEN}"
							if [ "$hour" -eq "0" ] && [ "$min" -eq "0" ]; then
						echo -ne "${YELLOW}"
							fi
							if [ "$hour" -eq "0" ] && [ "$min" -eq "0" ] && [ "$sec" -le "10" ]; then
								echo -ne "${RED}"
							fi
							echo -ne "$(printf "%02d" $hour):$(printf "%02d" $min):$(printf "%02d" $sec)\033[0K\r"
							let "sec=sec-1"
							sleep 1

						done
						sec=59
						let "min=min-1"
					done
					min=59
					let "hour=hour-1"

					echo -ne '\n'

					## (8) sox: Swiss Army knife of sound processing (shows equalizer)
					play "$song"

					## (8) mpv: Play song of choice (shows album art and seek controls)
					#mpv "$song"

					## (8) afplay: Apple Audio File Play (shows only time, without controls)
					#afplay "$song"

					## (9) Iteration: Increment i (Study session $i is finished)
					((i=i+1))

					## (9) Iteration: Calculate & add $session (Study session) to $dur (Duration)
					## e.g. session = 1 * 2 = 2 min; 1 * 0 = 0 min
					((session=i*dur))

					## (9) Iteration: Store $session outside the case option loop to avoid it from resetting to zero (or null)
					((total=total+$dur))

					## (9) Iteration: Current total: Studied for $total minute(s), consisting of $i session(s)
					## Store $i: amount of loops = amount of study sessions
					## Store $dur: durationo of $i study session
					## Store session=i*$dur minutes = duration of session $i
					## Store total=total+$dur = total sum of all study sessions (to be stored outside the case option, to avoid resetting its value)
					## Repeat loop, keep total until exit
					## Exit: multiply $i*$ession for study duration & save outside the case option loop
					echo -ne "${YELLOW}" '\nCurrent total: '"${GREEN}"'Studied for '"${RED}"$total 'minute'\('s'\)', '"${RED}""${GREEN}"'consisting of '"${RED}"$i' session'\('s'\)'.'"${GREEN}"'\n'"${RESET}"

				done

			fi

	esac


done

echo -ne "${RESET}"

## (0) tput: Set the cursor to its normal state
tput cnorm
